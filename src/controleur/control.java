package controleur;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class control {
    
    int adulte;
    int enfant;
    int etud;
    float resultat;
    float moyen;
    boolean promo = false;
    
    
    public void calcul() {
        if(promo == false) {
            setResultat((float) (7*adulte+5.5*etud+4*enfant));
        }
        else {
            setResultat((float) ((7*adulte+5.5*etud+4*enfant)*0.80));
        }
        setMoyen((float) resultat/(adulte+etud+enfant));
    }
    
    
    
    public static final String PROP_PROMO = "promo";

    public boolean isPromo() {
        return promo;
    }

    public void setPromo(boolean promo) {
        boolean oldPromo = this.promo;
        this.promo = promo;
        propertyChangeSupport.firePropertyChange(PROP_PROMO, oldPromo, promo);
    } 
    
    public static final String PROP_MOYEN = "moyen";

    public float getMoyen() {
        return moyen;
    }

    public void setMoyen(float moyen) {
        float oldMoyen = this.moyen;
        this.moyen = moyen;
        propertyChangeSupport.firePropertyChange(PROP_MOYEN, oldMoyen, moyen);
    }

    
    public static final String PROP_RESULTAT = "resultat";

    public float getResultat() {
        return resultat;
    }

    public void setResultat(float resultat) {
        float oldResultat = this.resultat;
        this.resultat = resultat;
        propertyChangeSupport.firePropertyChange(PROP_RESULTAT, oldResultat, resultat);
    }


    public static final String PROP_ETUD = "etud";

    public int getEtud() {
        return etud;
    }

    public void setEtud(int etud) {
        int oldEtud = this.etud;
        this.etud = etud;
        propertyChangeSupport.firePropertyChange(PROP_ETUD, oldEtud, etud);
    }

    public static final String PROP_ENFANT = "enfant";

    public int getEnfant() {
        return enfant;
    }

    public void setEnfant(int enfant) {
        int oldEnfant = this.enfant;
        this.enfant = enfant;
        propertyChangeSupport.firePropertyChange(PROP_ENFANT, oldEnfant, enfant);
    }

    public static final String PROP_ADULTE = "adulte";

    public int getAdulte() {
        return adulte;
    }

    public void setAdulte(int adulte) {
        int oldAdulte = this.adulte;
        this.adulte = adulte;
        propertyChangeSupport.firePropertyChange(PROP_ADULTE, oldAdulte, adulte);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

}
